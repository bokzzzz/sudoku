﻿using System;

namespace Sudoku.DTO
{
    class Game
    {
        private int idGame;
        private String level;
        private String bestTime;
        private int timePlayed;

        public int IdGame { get => idGame; set => idGame = value; }
        public string Level { get => level; set => level = value; }
        public string BestTime { get => bestTime; set => bestTime = value; }
        public int TimePlayed { get => timePlayed; set => timePlayed = value; }
    }
}
