﻿using FootApp;
using Sudoku.DAO;
using Sudoku.DTO;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace Sudoku
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer timer;
        private int time = 0;
        private int numberOfFillFields = 0;
        private String bestTimeGame;
        private Game currentGame;
        public MainWindow()
        {
            InitializeComponent();
            DatabaseConnection databaseConnection = DatabaseConnection.GetConnection();
            HideComponent();
            ResizeMode = ResizeMode.NoResize;
            ResizeMode = ResizeMode.CanMinimize;
            for (int i = 0; i < 9; i++)
                for (int j = 0; j < 9; j++)
                {
                    TextBox text = new TextBox();
                    text.TextAlignment = TextAlignment.Center;
                    text.FontSize = 35;
                    text.BorderBrush = Brushes.CornflowerBlue;
                    text.Background = Brushes.LightGoldenrodYellow;
                    Thickness tic = new Thickness(1, 1, 1, 1);
                    if (i == 0)
                        tic.Top = 5;
                    if (j == 0)
                        tic.Left = 5;
                    if (j == 8)
                        tic.Right = 5;
                    if (i == 8)
                        tic.Bottom = 5;
                    if (i == 2 || i == 5) tic.Bottom = 2;
                    if (j == 2 || j == 5) tic.Right = 2;
                    text.BorderThickness = tic;
                    text.PreviewTextInput += TextBox_PreviewTextInput;
                    text.PreviewKeyDown += TextBox_PreviewKeyDown;
                    text.TextChanged += TextBox_TextChanged;
                    gridField.Children.Add(text);
                    Grid.SetRow(text, i);
                    Grid.SetColumn(text, j);
                }
            gridField.IsEnabled=false;
        }
        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (box.Text.Length > 0) e.Handled = true;
            else
            {
                Regex regex = new Regex("[^1-9]");
                e.Handled = regex.IsMatch(e.Text);
            }
        }

        private void NewGameButton_Click(object sender, RoutedEventArgs e)
        {
            RestartFields();
            GameDAO gameDAO = new GameDAO();
            Game game = gameDAO.GetRandomGameByLevel(levelLabel.Content as String);
            if (game != null)
            {
                currentGame = game;
                timePlayedLabel.Content = "Time Played: " + game.TimePlayed;
                bestTimeLabel.Content = "Best Time: " + game.BestTime;
                bestTimeGame = game.BestTime;
                pauseButton.Visibility = Visibility.Visible;
                gridField.IsEnabled = true;
                time = 0;
                 
                var fields = new FieldDAO().GetFieldsByGame(game);
                numberOfFillFields = 0;
                gameDAO.UpdateTimePlayedGame(game);
                foreach (Field field in fields)
                {
                    TextBox textBox = gridField.Children[field.Row * 9 + field.Column] as TextBox;
                    textBox.Text = field.Value.ToString();
                    textBox.Focusable = false;
                }
                SetTimer();
                ShowComponent();
            }
        }

        private void RestartFields()
        {
            for (int i = 0; i < 9; i++)
                for (int j = 0; j < 9; j++)
                {
                    TextBox text = gridField.Children[i * 9 + j] as TextBox;
                    text.Text = "";
                    text.Background = Brushes.LightGoldenrodYellow;
                    text.Focusable = true;
                }
        }

        private void SetTimer()
        {
            if (timer != null) timer.Stop();
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();
            timer.Stop();
            timer.Start();
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            time++;
            int hours = time / 3600;
            int remainder = time - hours * 3600;
            int mins = remainder / 60;
            remainder = remainder - mins * 60;
            int secs = remainder;
            timeLabel.Content = (hours < 10 ? "0" + hours : hours + "") + ":" + (mins < 10 ? "0" + mins : mins + "") + ":" + (secs < 10 ? "0" + secs : secs + "");
        }

        private bool SetEqualNumberRowAndColumn(int row, int column, String value)
        {
            bool isSet = false;
            for (int i = 0; i < 9; i++)
            {
                TextBox text = gridField.Children[row * 9 + i] as TextBox;
                if (text.Text.Equals(value) && i != column)
                {
                    text.Background = Brushes.Red;
                    isSet = true;
                }

            }
            for (int i = 0; i < 9; i++)
            {
                TextBox text = gridField.Children[i * 9 + column] as TextBox;
                if (text.Text.Equals(value) && i != row)
                {
                    text.Background = Brushes.Red;
                    isSet = true;
                }

            }
            return isSet;
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete || e.Key == Key.Back)
            {
                TextBox textBox = sender as TextBox;
                textBox.Background = Brushes.LightGoldenrodYellow;
                String tmp = textBox.Text;
                textBox.Text = "";
                for (int i = 0; i < 9; i++)
                    for (int j = 0; j < 9; j++)
                    {
                        TextBox text = gridField.Children[i * 9 + j] as TextBox;
                        if (text.Equals(textBox))
                        {
                            RemoveAllTextBoxRedBack(i, j, tmp);
                            RemoveRedFromMiniSquare(i, j, tmp);
                            break;
                        }
                    }

            }
        }
        private void RemoveAllTextBoxRedBack(int row, int column, String value)
        {
            int numbeOfEquals = 0, xRow = 0, xColumn = 0;
            TextBox textBoxRow = null;
            TextBox textBoxColumn = null;
            for (int i = 0; i < 9; i++)
            {
                TextBox text = gridField.Children[i * 9 + column] as TextBox;
                if (text.Text.Equals(value) && i != row)
                {
                    numbeOfEquals++;
                    textBoxRow = text;
                    xRow = i;
                    xColumn = column;
                    if (numbeOfEquals > 1) break;
                }

            }
            if (numbeOfEquals == 1)
            {
                int res = RowRemove(xRow, xColumn, value) + ColumnRemove(xRow, xColumn, value)+
                    NumberOfRemoveMiniInSquare(ref xRow, ref xColumn, value); ;
                if (res == 0)
                    textBoxRow.Background = Brushes.LightGoldenrodYellow;
            }
            numbeOfEquals = 0;
            for (int i = 0; i < 9; i++)
            {
                TextBox text = gridField.Children[row * 9 + i] as TextBox;
                if (text.Text.Equals(value) && i != column)
                {
                    numbeOfEquals++;
                    textBoxColumn = text;
                    xRow = row;
                    xColumn = i;
                }
            }
            if (numbeOfEquals == 1)
            {
                int res = RowRemove(xRow, xColumn, value)+ ColumnRemove(xRow, xColumn, value)
                    +NumberOfRemoveMiniInSquare(ref xRow,ref xColumn, value);
                if (res == 0)
                {
                    textBoxColumn.Background = Brushes.LightGoldenrodYellow;
                }
            }
        }
        private int RowRemove(int row, int column, String value)
        {
            int numbeOfEquals = 0;
            for (int i = 0; i < 9; i++)
            {
                TextBox text = gridField.Children[i * 9 + column] as TextBox;
                if (text.Text.Equals(value) && i != row)
                {
                    numbeOfEquals++;
                }

            }
            return numbeOfEquals;
        }
        private int ColumnRemove(int row, int column, String value)
        {
            int numbeOfEquals = 0;
            for (int i = 0; i < 9; i++)
            {
                TextBox text = gridField.Children[row * 9 + i] as TextBox;
                if (text.Text.Equals(value) && i != column)
                {
                    numbeOfEquals++;
                }
            }
            return numbeOfEquals;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            if (slider.Value == 1) levelLabel.Content = "Easy";
            else if (slider.Value == 2) levelLabel.Content = "Medium";
            else if (slider.Value == 3) levelLabel.Content = "Hard";
            else levelLabel.Content = "Expert";

        }
        private void HideComponent()
        {
            bestTimeLabel.Visibility = Visibility.Hidden;
            timeLabel.Visibility = Visibility.Hidden;
            image.Visibility = Visibility.Hidden;
            timePlayedLabel.Visibility = Visibility.Hidden;

        }
        private void ShowComponent()
        {
            bestTimeLabel.Visibility = Visibility.Visible;
            timeLabel.Visibility = Visibility.Visible;
            image.Visibility = Visibility.Visible;
            timePlayedLabel.Visibility = Visibility.Visible;
        }

        private void pauseButton_Click(object sender, RoutedEventArgs e)
        {
            if (timer.IsEnabled)
            {
                PauseImgVisibility();
            }
            else
            {
                ResumeImgVisibility();
            }
        }

        private void imgButton_Click(object sender, RoutedEventArgs e)
        {
            if (!timer.IsEnabled)
            {
                ResumeImgVisibility();
            }
        }
        private void ResumeImgVisibility()
        {
            timer.Start();
            pauseButton.Content = "Pause";
            pauseGrid.Visibility = Visibility.Hidden;
            gridField.Visibility = Visibility.Visible;
        }
        private void PauseImgVisibility()
        {
            timer.Stop();
            pauseButton.Content = "Resume";
            pauseGrid.Visibility = Visibility.Visible;
            gridField.Visibility = Visibility.Hidden;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (string.IsNullOrEmpty(textBox.Text))
            {
                numberOfFillFields--;
            }
            else
            {
                numberOfFillFields++;
                for (int i = 0; i < 9; i++)
                    for (int j = 0; j < 9; j++)
                    {
                        TextBox text = gridField.Children[i * 9 + j] as TextBox;
                        if (text.Equals(textBox) && !string.IsNullOrEmpty(text.Text))
                        {

                           
                            if (SetEqualNumberRowAndColumn(i, j, text.Text))
                            {
                                textBox.Background = Brushes.Red;
                            }
                            if(CheckNumberInMiniSquare(i, j, text.Text))
                                textBox.Background = Brushes.Red;
                        }
                    }
                if (numberOfFillFields == 81)
                {
                    if (CheckIsWin())
                    {
                        timer.Stop();
                        MessageBox.Show("Congratulations, You Just Won The Game", "Sudoku", MessageBoxButton.OK, MessageBoxImage.Information);
                        CheckBestTime();
                    }
                    else
                    {
                        timer.Stop();
                        MessageBox.Show("Congratulations, You Just Lost The Game", "Sudoku",  MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    gridField.IsEnabled = false;
                    pauseButton.Visibility = Visibility.Hidden;
                    

                }
            }
        }

        private void CheckBestTime()
        {
            long bestTime = FromStringToTime(bestTimeGame);
            if (time < bestTime)
            {
                int hours = time / 3600;
                int remainder = time - hours * 3600;
                int mins = remainder / 60;
                remainder = remainder - mins * 60;
                int secs = remainder;
                currentGame.BestTime= (hours < 10 ? "0" + hours : hours + "") + ":" + (mins < 10 ? "0" + mins : mins + "") + ":" + (secs < 10 ? "0" + secs : secs + "");
                new GameDAO().UpdateBestTimeGame(currentGame);
                bestTimeLabel.Content = "Best Time: " + currentGame.BestTime;
                MessageBox.Show("You set a best record for this game : " + currentGame.BestTime);
            }
        }

        private long FromStringToTime(string bestTimeGame)
        {
            long time=0;
            String[] tmpTime = bestTimeGame.Split(':');
            time += (Int64.Parse(tmpTime[0]) * 3600);
            time += (Int64.Parse(tmpTime[1]) * 60);
            time += Int64.Parse(tmpTime[2]);
            return time;
        }

        private bool CheckIsWin()
        {
            for (int i = 0; i < 9; i++)
                for (int j = 0; j < 9; j++)
                {
                    TextBox text = gridField.Children[i * 9 + j] as TextBox;
                    if (text.Background == Brushes.Red)
                    {

                        return false;
                    }
                }
            return true;
        }
        private bool CheckNumberInMiniSquare(int row,int column,String value)
        {
            int rowDistanceForward = 2 - row % 3;
            int rowDistanceBackward = row % 3;
            int columnDistanceForward = 2 - column % 3;
            int columnDistanceBackward =column % 3;
            int numberOfEquals = 0;
            for(int i = row -rowDistanceBackward; i <= row + rowDistanceForward; i++)
            {
                for(int j=column - columnDistanceBackward;j<= column + columnDistanceForward; j++)
                {
                    TextBox textBox = gridField.Children[i * 9 + j] as TextBox;
                    if (textBox.Text.Equals(value) && i!= row && j != column)
                    {
                        
                        textBox.Background = Brushes.Red;
                        numberOfEquals++;
                    }
                }
            }
            if (numberOfEquals > 0) return true;
            return false;
        }
        private bool RemoveRedFromMiniSquare(int row, int column, String value)
        {
            int xRow = row,xColumn=column;
            int numberOfEquals = NumberOfRemoveMiniInSquare(ref xRow, ref xColumn, value);
            if(numberOfEquals == 1)
            {
                int tmpCount = RowRemove(xRow, xColumn, value) + ColumnRemove(xRow, xColumn, value);
                if (tmpCount == 0)
                {
                    TextBox textBox = gridField.Children[xRow * 9 + xColumn] as TextBox;
                    textBox.Background = Brushes.LightGoldenrodYellow;
                }
                return true;
            }
            return false;
        }
        private int NumberOfRemoveMiniInSquare(ref int row,ref int column, String value)
        {
            int rowDistanceForward = 2 - row % 3;
            int rowDistanceBackward = row % 3;
            int columnDistanceForward = 2 - column % 3;
            int columnDistanceBackward = column % 3;
            int numberOfEquals = 0;
            int xRow = row, xColumn = column;
            for (int i = xRow - rowDistanceBackward; i <= xRow + rowDistanceForward; i++)
            {
                for (int j = xColumn - columnDistanceBackward; j <= xColumn + columnDistanceForward; j++)
                {
                    TextBox textBox = gridField.Children[i * 9 + j] as TextBox;
                    if (textBox.Text.Equals(value) && (i != xRow || j != xColumn))
                    {
                        numberOfEquals++;
                        row = i;
                        column = j;
                        if (numberOfEquals > 1) break;
                    }
                }
            }
            return numberOfEquals;
        }
    }
}
