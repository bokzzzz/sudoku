﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku.DTO
{
    class Field
    {
        private int row;
        private int column;
        private int value;

        public int Row { get => row; set => row = value; }
        public int Column { get => column; set => column = value; }
        public int Value { get => value; set => this.value = value; }
    }
}
