﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp
{
    class DatabaseConnection
    {
        private static MySqlConnection MySqlConnection;
        private static DatabaseConnection Connection;

        private static string dbServer;
        private static string databaseName;
        private static string username;
        private static string password;
        private static string port;
        
        static DatabaseConnection() 
        {
            dbServer = ConfigurationManager.AppSettings["DatebaseServer"];
            databaseName = ConfigurationManager.AppSettings["DatebaseName"];
            username = ConfigurationManager.AppSettings["Username"];
            password = ConfigurationManager.AppSettings["Password"];
            port = ConfigurationManager.AppSettings["Port"];

        }

        
            
        
        private DatabaseConnection()
        {
                MySqlConnection = new MySqlConnection("Server=" + dbServer + ";Database=" + databaseName +";Port="+port+ ";UserId=" + username + ";Password=" + password);
        }
        public static DatabaseConnection GetConnection()
        {
            if (Connection == null)
            {
                Connection = new DatabaseConnection();
                MySqlConnection.Open();
            }
            return Connection;
        }
        public MySqlCommand CreateCommand()
        {
            return MySqlConnection.CreateCommand();
        }

        public void CloseConnection()
        {
            MySqlConnection.Close();
            MySqlConnection = null;
        }
    }
}
