﻿using FootApp;
using MySql.Data.MySqlClient;
using Sudoku.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku.DAO
{
    class FieldDAO
    {
        public List<Field> GetFieldsByGame(Game game)
        {
            List<Field> fields = new List<Field>();
            MySqlCommand sqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            sqlCommand.CommandText = "select * from Field where idGame=@id";
            sqlCommand.Parameters.AddWithValue("@id",game.IdGame);
            sqlCommand.Prepare();
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                fields.Add(new Field 
                { 
                Column=reader.GetInt32("column"),
                Row=reader.GetInt32("row"),
                Value= reader.GetInt32("value")
                }
                );
            }
            reader.Close();
            return fields;
        }
    }
}
