﻿using FootApp;
using MySql.Data.MySqlClient;
using Sudoku.DTO;
using System;
using System.Collections.Generic;

namespace Sudoku.DAO
{
    class GameDAO
    {
        public List<Game> GetGames()
        {
            List<Game> games = new List<Game>();
            MySqlCommand sqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            sqlCommand.CommandText = "select * from Game";
            MySqlDataReader reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                games.Add(new Game
                {
                    BestTime= reader.IsDBNull(1) ? "N/A" : reader.GetString("bestTime"),
                    Level= reader.GetString("level"),
                    IdGame=reader.GetInt32("idGame"),
                    TimePlayed = reader.GetInt32("timePlayed")

                });
            }
            reader.Close();

            return games;
        }
        public int GetNumberOfGamesByLevel(String level)
        {
            List<Game> games = new List<Game>();
            MySqlCommand sqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            sqlCommand.CommandText = "select count(*) as number from Game where level=@level";
            sqlCommand.Parameters.AddWithValue("@level", level);
            sqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    int number = reader.GetInt32("number");
                    reader.Close();
                    return number;
                }
               
            }catch(Exception)
            {
                return -1;
            }
            return -1;
        }
        public Game GetRandomGameByLevel(String level)
        {
            Random rnd = new Random();
            int gameNumber = rnd.Next(GetNumberOfGamesByLevel(level));
            MySqlCommand sqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            sqlCommand.CommandText = "select *  from Game where level=@level limit 1 offset @offset";
            sqlCommand.Parameters.AddWithValue("@level", level);
            sqlCommand.Parameters.AddWithValue("@offset", gameNumber);
            sqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = sqlCommand.ExecuteReader();
                Game game=null;
                if (reader.Read())
                {
                   game = new Game
                    {
                        BestTime = reader.IsDBNull(1) ? "N/A" : reader.GetString("bestTime"),
                        Level = reader.GetString("level"),
                        IdGame = reader.GetInt32("idGame"),
                        TimePlayed = reader.GetInt32("timePlayed")

                    };
                }
                reader.Close();
                return game;
            } catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return null;
            }
        }
       
        public bool UpdateTimePlayedGame(Game game)
        {
            MySqlCommand sqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            sqlCommand.CommandText = " Update game set timePlayed=timePlayed +1 where idGame=@id;";
            sqlCommand.Parameters.AddWithValue("@id", game.IdGame);
            sqlCommand.Prepare();
            try
            {
                sqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateBestTimeGame(Game game)
        {
            MySqlCommand sqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            sqlCommand.CommandText = " Update game set bestTime=@bestTime where idGame=@id;";
            sqlCommand.Parameters.AddWithValue("@id", game.IdGame);
            sqlCommand.Parameters.AddWithValue("@bestTime", game.BestTime);
            sqlCommand.Prepare();
            try
            {
                sqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
